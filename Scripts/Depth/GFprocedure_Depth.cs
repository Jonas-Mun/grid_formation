﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Depth
{
    public static class GFprocedure
    {
        public static List<Vector3> PositionsOnLine(float distBtxtUnits, int totalUnits, Vector3 I)
        {
            List<Vector3> positions = new List<Vector3>();

            for (int currIndexUnit = 0; currIndexUnit < totalUnits; currIndexUnit++)
            {
                float offset = distBtxtUnits * currIndexUnit;
                Vector3 offsetVec = new Vector3(offset, 0, 0);  // Change acrros XAxis.
                Vector3 curPos = I + offsetVec;

                positions.Add(curPos);
            }
            return positions;
        }

        public static List<Vector3> PositionsOnRadius(Vector3 I, Vector3 E, float sizeOfUnit, float gap, float angle, int maxUnits)
        {
            float radius = Vector3.Magnitude(E - I);
            int totalFrontUnits = GFequation.TotalUnits(radius, sizeOfUnit, gap);

            List<Vector3> positions = new List<Vector3>();

            for (int i = 0; i < totalFrontUnits; i++)
            {
                if (i >= maxUnits)
                {
                    return positions;
                }
                Vector3 currentPosition = GFequation.PositionOnRadius(I, sizeOfUnit, gap, angle, i);
                positions.Add(currentPosition);
            }
            float distBtxtUnit = sizeOfUnit + gap;
            positions = DepthPositions(positions, distBtxtUnit, totalFrontUnits, maxUnits, angle);

            return positions;
        }

        public static List<Vector3> DepthPositions(List<Vector3> positions, float distBtxtUnit, int totalFrontUnits, int totalUnits, float angle)
        {
            int firstUnitOnDepth = totalFrontUnits;
            for (int frontI = 0, depthI = firstUnitOnDepth; depthI < totalUnits; frontI++, depthI++)
            {
                Vector3 F = positions[frontI];
                Vector3 depthPosition = GFequation.DepthUnitPosition(F, angle, distBtxtUnit);
                positions.Add(depthPosition);
            }
            return positions;
        }
    }
}
    


