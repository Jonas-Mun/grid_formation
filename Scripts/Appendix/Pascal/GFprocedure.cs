﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pascal {
	public static class GFprocedure
    {
        public static List<Vector3> PositionsOnLine(float distBtxtUnits, int totalUnits, Vector3 I)
        {
            List<Vector3> positions = new List<Vector3>();

            for (int currIndexUnit = 0; currIndexUnit < totalUnits; currIndexUnit++)
            {
                float offset = distBtxtUnits * currIndexUnit;
                Vector3 offsetVec = new Vector3(offset, 0, 0);  // Change acrros XAxis.
                Vector3 curPos = I + offsetVec;

                positions.Add(curPos);
            }
            return positions;
        }

		public static List<Vector3> PositionsOnRadius(Vector3 I, Vector3 E, float sizeOfUnit, float gap, float angle, int maxUnits)
		{
			float radius = Vector3.Magnitude(E - I);
			int totalFrontUnits = GFequation.TotalUnits(radius, sizeOfUnit, gap);

			List<Vector3> positions = new List<Vector3>();

			for (int i = 0; i < totalFrontUnits; i++)
			{
				if (i >= maxUnits)
				{
					return positions;
				}
				Vector3 currentPosition = GFequation.PositionOnRadius(I, sizeOfUnit, gap, angle, i);
				positions.Add(currentPosition);
			}

			return positions;
		}

		public static List<Vector3> DepthPositions(List<Vector3> positions, float distBtxtUnit, int totalFrontUnits, int totalUnits, float angle)
		{
			int firstUnitOnDepth = totalFrontUnits;
			for(int frontI = 0, depthI = firstUnitOnDepth; depthI < totalUnits; frontI++, depthI++)
			{
				Vector3 F = positions[frontI];
				Vector3 depthPosition = GFequation.DepthUnitPosition(F, angle, distBtxtUnit);
				positions.Add(depthPosition);
			}
			return positions;
		}

		

		public static List<Vector3> GridFormationPositions(Vector3 I, Vector3 E, float sizeOfUnit, float gap, int maxUnits, float angle)
		{
			float radius = Vector3.Magnitude(E - I);
			int totalFrontUnits = GFequation.TotalUnits(radius, sizeOfUnit, gap);
			float distBtxtUnit = sizeOfUnit + gap;

			List<Vector3> gridPositions = new List<Vector3>();
			gridPositions = PositionsOnRadius(I, E, sizeOfUnit, gap, angle, maxUnits);
			gridPositions = DepthPositions(gridPositions, distBtxtUnit, totalFrontUnits, maxUnits, angle);

			return gridPositions;
		}

		public static List<Vector3> GeneralPositions(Vector3 I, Vector3 E, float sizeOfUnit, float gap, int maxUnits, float angle)
		{
			float radius = Vector3.Magnitude(E - I);
			int unitsOnRadius = GFequation.TotalUnits(radius, sizeOfUnit, gap);
			int totalDepths = GFequation.TotalDepth(maxUnits, unitsOnRadius);

			float distBtxtUnit = sizeOfUnit + gap;

			// No Depth
			// --------
			if (totalDepths == 0)
			{
				return PositionsOnRadius(I, E, sizeOfUnit, gap, angle, maxUnits);   // On the radius
			}
			
			// Depth
			// -----
			Vector3 curI = I;
			List<Vector3> positions = new List<Vector3>();

			// Main Procedure
			// --------------
			for (int curDepth = 0, numUnits = 0; curDepth <= totalDepths; curDepth++) {

				for (int i = 0; i < unitsOnRadius && numUnits < maxUnits; i++, numUnits++)
				{
					Vector3 position = GFequation.PositionOnRadius(curI, sizeOfUnit, gap, angle, i);
					positions.Add(position);
				}
				curI = GFequation.DepthUnitPosition(curI, angle, distBtxtUnit);     // Go down by to I depth
			}

			return positions;

		}

		public static List<Vector3> GeneralPositionsPascal(Vector3 I, Vector3 E, float sizeOfUnit, float gap, int maxUnits, float angle)
		{
			float radius = Vector3.Magnitude(E - I);
			int unitsOnRadius = GFequation.TotalUnits(radius, sizeOfUnit, gap);
			int totalDepths = GFequation.TotalDepth(maxUnits, unitsOnRadius);
			int remainder = maxUnits % unitsOnRadius;

			float distBtxtUnit = sizeOfUnit + gap;

			// No Depth
			// --------
			if (totalDepths == 0)
			{
				return PositionsOnRadius(I, E, sizeOfUnit, gap, angle, maxUnits);   // On the radius
			}

			// Depth
			// -----
			Vector3 curI = I;
			List<Vector3> positions = new List<Vector3>();

			// Main Procedure
			// --------------
			for (int curDepth = 0, numUnits = 0; curDepth <= totalDepths; curDepth++)
			{
				// Apply Pascal
				if (curDepth == totalDepths && remainder > 0)
				{
					/** Fix moving row **/
					Vector3 end = GFequation.PositionOnRadius(I, sizeOfUnit, gap, angle, unitsOnRadius - 1);

					int unitsRemaining = maxUnits % unitsOnRadius;
					List<Vector3> pPascal = PascalsPositions(I, end, distBtxtUnit, curDepth, angle, unitsRemaining);
					positions.AddRange(pPascal);
					return positions;
				}
				for (int i = 0; i < unitsOnRadius && numUnits < maxUnits; i++, numUnits++)
				{
					Vector3 position = GFequation.PositionOnRadius(curI, sizeOfUnit, gap, angle, i);
					positions.Add(position);
				}
				curI = GFequation.DepthUnitPosition(curI, angle, distBtxtUnit);     // Go down by to I depth
			}

			return positions;

		}

		private static List<Vector3> PascalsPositions(Vector3 I, Vector3 E, float distBtxtUnit, int curDepth, float angle, int unitsRemaining)
		{
			// Mid Point
			// ---------
			Vector3 halfRadius = (E - I) / 2;
			Vector3 midPoint = I + halfRadius;

			// Pascal's Center
			// ---------------
			Vector3 center = GFequation.PascalsCenter(midPoint, distBtxtUnit, curDepth, angle);
			Debug.Log("Center: " + center);

			// Pascal's End
			// ------------
			Vector3 end = GFequation.PascalsEnd(center, distBtxtUnit, unitsRemaining, angle);

			// CalculatePositions
			// ------------------
			List<Vector3> positions = new List<Vector3>();
			for (int i = 0; i <= unitsRemaining; i++)
			{
				Vector3 position = GFequation.PascalsPosition(end, distBtxtUnit, angle, i);
				positions.Add(position);
			}

			return positions;
		}
	}	
}

    


