﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pascal {
	public static class GFequation 
	{
		public static int TotalUnits(float lengthOfLine, float sizeOfUnit, float gap)
		{
			int units = (int) (lengthOfLine / (sizeOfUnit + gap));

			return units + 1;
		}

		public static int TotalDepth(int maxUnits, int totalFrontUnits)
		{
			int fullDepths = (maxUnits / totalFrontUnits) -1;
			int leftOverDepth = 0;

			// Remainder conveys left over units in a row
			if (maxUnits % totalFrontUnits > 0)
			{
				leftOverDepth = 1;
			}

			return fullDepths + leftOverDepth;
		}

		public static Vector3 PositionOnLine(Vector3 I, float d, int i)
		{
			float offset = d * i;

			Vector3 position = I;
			position.x = I.x + offset;

			return position;
		}

		public static Vector3 PositionOnRadius(Vector3 I, float sizeOfUnit, float gap, float angle, int index)
		{
			float distBtxtUnit = sizeOfUnit + gap;
			Vector3 offset = new Vector3(distBtxtUnit * Mathf.Cos(angle * Mathf.Deg2Rad), 0f, -distBtxtUnit * Mathf.Sin(angle * Mathf.Deg2Rad));
			
			Vector3 position = I + (offset * index);
			Debug.Log(position);

			return position;
		}

		public static Vector3 DepthUnitPosition(Vector3 F, float angle, float distBtxtUnit)
		{
			Debug.Log("[Angle] " + angle);
			Debug.Log("[Sin(angle)] " + Mathf.Sin(angle * Mathf.Deg2Rad));
			Debug.Log("[Cos(angle)] " + Mathf.Cos(angle * Mathf.Deg2Rad));
			Vector3 offset = new Vector3 (-Mathf.Sin(angle * Mathf.Deg2Rad), 0f, -Mathf.Cos((angle * Mathf.Deg2Rad))) * distBtxtUnit;
			Vector3 position = F + offset;

			return position;
		}

		/** Total War Like **/
		public static Vector3 PascalsCenter(Vector3 midPoint, float distBtxtUnit, int currentDepth, float angle)
		{
			Vector3 offset = new Vector3(-Mathf.Sin(angle * Mathf.Deg2Rad), 0f, -Mathf.Cos(angle * Mathf.Deg2Rad));
			offset *= distBtxtUnit * currentDepth;

			Vector3 center = midPoint + offset;

			return center;
		}

		public static Vector3 PascalsEnd(Vector3 center, float distBtxtUnit, int unitsRemaining, float angle)
		{
			float shift = distBtxtUnit * (unitsRemaining - 1);
			shift = shift / 2;

			Vector3 offset =  new Vector3(Mathf.Cos(angle * Mathf.Deg2Rad), 0f, -Mathf.Sin(angle * Mathf.Deg2Rad)); // must point opposit direction. Negative inverts the cubes
			offset *= shift;

			Vector3 end = center - offset;
			return end;
		}

		public static Vector3 PascalsPosition(Vector3 end, float distBtxtUnit, float angle, int index)
		{
			Vector3 offset = new Vector3(Mathf.Cos(angle * Mathf.Deg2Rad), 0f, -Mathf.Sin(angle * Mathf.Deg2Rad)); // negate Z to inverse. Due to Unity implementation
			offset *= distBtxtUnit;

			return end + (offset * index);
		}
    
	}
}


