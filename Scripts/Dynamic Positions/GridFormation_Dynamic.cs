﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DynamicPositions
{
    public class GridFormation : MonoBehaviour
    {
        public Camera mainCam;
        public LayerMask groundMask;
        public GameObject prefab;
        public GameObject guide;

        private bool dragging;
        int rightMButton = 1;

        [SerializeField]
        float gap;
        [SerializeField]
        float sizeOfUnit;

        Vector3 I;
        Vector3 E;
        Vector3 C;

        float angle = 0;
        float radius = 0;

        private void Update()
        {
            Dragging(I, C);
            if (dragging)
            {
                RotateGuide();
                C = ClickedCoord();
                float angledAligned = guide.transform.rotation.eulerAngles.y - 90;
                float newRadius = Vector3.Magnitude(C - I);
                if (angle != angledAligned || radius != newRadius)
                {
                    angle = angledAligned;
                    radius = newRadius;

                    float distBtxtUnit = sizeOfUnit + gap;

                    int totalUnits = GFequation.TotalUnits(radius, sizeOfUnit, gap);

                    List<Vector3> positions = GFprocedure.PositionsOnRadius(I, C, sizeOfUnit, gap, angle);
                    int totalUnitsPlaced = PlaceUnits(positions);
                }



                //Debug.Log("Dragging");
            }
        }

        /** Dragging Mechanisms**/
        private void Dragging(Vector3 I, Vector3 E)
        {
            if (Input.GetMouseButtonDown(rightMButton))
            {
                dragging = true;
                this.I = ClickedCoord();
                Debug.Log(I);
                guide.transform.localPosition = this.I;
            }

            if (Input.GetMouseButtonUp(rightMButton))
            {
                dragging = false;
                this.E = ClickedCoord();
                // = Vector3.Magnitude(E - I);


            }
        }

        private Vector3 ClickedCoord()
        {
            Ray ray = mainCam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            Vector3 clickedCoord = Vector3.zero;

            if (Physics.Raycast(ray, out hit, 100.0f, groundMask))
            {
                clickedCoord = hit.point;   //Coordinates of clicked position.
            }
            return clickedCoord;
        }

        private int PlaceUnits(List<Vector3> positions)
        {
            int units = 0;
            for (int i = 0; i < positions.Count; i++)
            {
                GameObject unit = Instantiate(prefab);
                unit.transform.position = positions[i];
                units++;
            }
            return units;
        }

        private void RotateGuide()
        {
            Ray cameraRay = mainCam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            //  Coordinate
            if (Physics.Raycast(cameraRay, out hit, 100.0f, groundMask))
            {

                Vector3 pointToLook = cameraRay.GetPoint(hit.distance);
                // Rotate parent Object
                guide.transform.LookAt(new Vector3(pointToLook.x, transform.position.y, pointToLook.z));
            }
        }

        private void debugLine(int totalUnitsPos, int totalUnitsfab)
        {
            Debug.Log("[Debug Units] Total Positions: " + totalUnitsPos);
            Debug.Log("[Debug Units] Total Units Prefab: " + totalUnitsfab);
        }
    }
}

