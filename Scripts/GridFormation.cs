﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    public class GridFormation : MonoBehaviour
    {
        public Camera mainCam;
        public LayerMask groundMask;
        public GameObject prefab;
        public GameObject guide;

        private bool dragging;
        int rightMButton = 1;

        [SerializeField]
        float gap;
        [SerializeField]
        float sizeOfUnit;

        Vector3 I;
        Vector3 E;
        Vector3 C;

        float angle = 0;
        float radius = 0;

        [SerializeField]
        private int maxUnits;

        List<Transform> units;

    bool depth;

        private void Start()
        {
        depth = true;
        units = new List<Transform>();
            FillUnits(maxUnits);
        }

        private void Update()
        {
            Dragging(I, C);
            if (dragging)
            {
                RotateGuide();
                C = ClickedCoord();
                float angledAligned = guide.transform.rotation.eulerAngles.y - 90;
                float newRadius = Vector3.Magnitude(C - I);
                if (angle != angledAligned || radius != newRadius)
                {
                    angle = angledAligned;
                    radius = newRadius;

                    List<Vector3> positions = GFprocedure.GridFormationPositions(I, C, sizeOfUnit, gap, maxUnits, angle);
                    int totalUnitsPlaced = PlaceUnits(positions);
                }
            }
        }

        /** Dragging Mechanisms**/
        private void Dragging(Vector3 I, Vector3 E)
        {
            if (Input.GetMouseButtonDown(rightMButton))
            {
                dragging = true;
                this.I = ClickedCoord();
                Debug.Log(I);
                guide.transform.localPosition = this.I;
            }

            if (Input.GetMouseButtonUp(rightMButton))
            {
                dragging = false;
                this.E = ClickedCoord();
                // = Vector3.Magnitude(E - I);


            }
        }

        private Vector3 ClickedCoord()
        {
            Ray ray = mainCam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            Vector3 clickedCoord = Vector3.zero;

            if (Physics.Raycast(ray, out hit, 100.0f, groundMask))
            {
                clickedCoord = hit.point;   //Coordinates of clicked position.
            }
            return clickedCoord;
        }

        private void FillUnits(int amount)
        {
            for(int i = 0; i < amount; i++)
            {
                GameObject unit = Instantiate(prefab);
                unit.SetActive(false);
                units.Add(unit.transform);
            }
        }

    private int PlaceUnits(List<Vector3> positions)
    {
        int unitIndex = 0;
        for (int i = 0; i < positions.Count; i++)   // use positions count for safety
        {
                
            Transform unit = units[i];
            unit.position = positions[i];
            unit.gameObject.SetActive(true);
            unitIndex++;
        }
        if (!depth)
        {
            DeactivateUnits(unitIndex);
        }
        
        return unitIndex;
    }

    private void DeactivateUnits(int index)
    {
        for (int i = index; i < units.Count; i++)
        {
            units[i].gameObject.SetActive(false);
        }
    }

    private void RotateGuide()
    {
        Ray cameraRay = mainCam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        //  Coordinate
        if (Physics.Raycast(cameraRay, out hit, 100.0f, groundMask))
        {
            Vector3 pointToLook = cameraRay.GetPoint(hit.distance);
            // Rotate parent Object
            guide.transform.LookAt(new Vector3(pointToLook.x, transform.position.y, pointToLook.z));
        }
    }

    private void debugLine(int totalUnitsPos, int totalUnitsfab)
    {
        Debug.Log("[Debug Units] Total Positions: " + totalUnitsPos);
        Debug.Log("[Debug Units] Total Units Prefab: " + totalUnitsfab);
    }
}

