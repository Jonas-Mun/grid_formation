﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FixedUnits
{
    public static class GFequation
    {
        public static int TotalUnits(float lengthOfLine, float sizeOfUnit, float gap)
        {
            int units = (int)(lengthOfLine / (sizeOfUnit + gap));

            return units + 1;
        }

        public static Vector3 PositionOnLine(Vector3 I, float d, int i)
        {
            float offset = d * i;

            Vector3 position = I;
            position.x = I.x + offset;

            return position;
        }

        public static Vector3 PositionOnRadius(Vector3 I, float sizeOfUnit, float gap, float angle, int index)
        {
            float distBtxtUnit = sizeOfUnit + gap;
            Vector3 offset = new Vector3(distBtxtUnit * Mathf.Cos(angle * Mathf.Deg2Rad), 0f, -distBtxtUnit * Mathf.Sin(angle * Mathf.Deg2Rad));

            Vector3 position = I + (offset * index);
            Debug.Log(position);

            return position;
        }
    }
}

