﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ImaginaryLine
{
    public static class GFprocedure
    {
        public static List<Vector3> PositionsOnLine(float distBtxtUnits, int totalUnits, Vector3 I)
        {
            List<Vector3> positions = new List<Vector3>();

            for (int currIndexUnit = 0; currIndexUnit < totalUnits; currIndexUnit++)
            {
                float offset = distBtxtUnits * currIndexUnit;
                Vector3 offsetVec = new Vector3(offset, 0, 0);  // Change acrros XAxis.
                Vector3 curPos = I + offsetVec;

                positions.Add(curPos);
            }
            return positions;
        }
    }
}

