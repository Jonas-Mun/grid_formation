﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ImaginaryLine
{
    public class GridFormation : MonoBehaviour
    {
        public Camera mainCam;
        public LayerMask groundMask;
        public GameObject prefab;

        private bool dragging;
        int rightMButton = 1;

        [SerializeField]
        float gap;
        [SerializeField]
        float sizeOfUnit;

        Vector3 I;
        Vector3 E;

        private void Update()
        {
            Dragging();
            if (dragging)
            {
                //Debug.Log("Dragging");
            }
        }

        /** Dragging Mechanisms**/
        private void Dragging()
        {
            if (Input.GetMouseButtonDown(rightMButton))
            {
                dragging = true;
                I = ClickedCoord();
                Debug.Log(I);
            }

            if (Input.GetMouseButtonUp(rightMButton))
            {
                dragging = false;
                E = ClickedCoord();
                // = Vector3.Magnitude(E - I);
                float lengthOfLine = Vector3.Magnitude(E - I);
                Debug.Log(lengthOfLine);
                float distBtxtUnit = sizeOfUnit + gap;

                if (E.x < I.x)
                {
                    distBtxtUnit = -distBtxtUnit;
                }

                int totalUnits = GFequation.TotalUnits(lengthOfLine, sizeOfUnit, gap);

                List<Vector3> positions = GFprocedure.PositionsOnLine(distBtxtUnit, totalUnits, I);
                int totalUnitsPlaced = PlaceUnits(positions);

                debugLine(positions.Count, totalUnitsPlaced);

                Debug.Log(E);
            }
        }

        private Vector3 ClickedCoord()
        {
            Ray ray = mainCam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            Vector3 clickedCoord = Vector3.zero;

            if (Physics.Raycast(ray, out hit, 100.0f, groundMask))
            {
                clickedCoord = hit.point;   //Coordinates of clicked position.
            }
            return clickedCoord;
        }

        private int PlaceUnits(List<Vector3> positions)
        {
            int units = 0;
            for (int i = 0; i < positions.Count; i++)
            {
                GameObject unit = Instantiate(prefab);
                unit.transform.position = positions[i];
                units++;
            }
            return units;
        }

        private void debugLine(int totalUnitsPos, int totalUnitsfab)
        {
            Debug.Log("[Debug Units] Total Positions: " + totalUnitsPos);
            Debug.Log("[Debug Units] Total Units Prefab: " + totalUnitsfab);
        }
    }
}

