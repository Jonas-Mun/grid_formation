﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UnitCircle
{
    public class GridFormation : MonoBehaviour
    {
        public Camera mainCam;
        public LayerMask groundMask;
        public GameObject prefab;
        public GameObject guide;

        private bool dragging;
        int rightMButton = 1;

        [SerializeField]
        float gap;
        [SerializeField]
        float sizeOfUnit;

        Vector3 I;
        Vector3 E;

        private void Update()
        {
            Dragging();
            if (dragging)
            {
                RotateGuide();
                //Debug.Log("Dragging");
            }
        }

        /** Dragging Mechanisms**/
        private void Dragging()
        {
            if (Input.GetMouseButtonDown(rightMButton))
            {
                dragging = true;
                I = ClickedCoord();
                Debug.Log(I);
                guide.transform.localPosition = I;
            }

            if (Input.GetMouseButtonUp(rightMButton))
            {
                dragging = false;
                E = ClickedCoord();
                // = Vector3.Magnitude(E - I);
                float lengthOfLine = Vector3.Magnitude(E - I);
                Debug.Log(lengthOfLine);
                float distBtxtUnit = sizeOfUnit + gap;

                if (E.x < I.x)
                {
                    distBtxtUnit = -distBtxtUnit;
                }

                int totalUnits = GFequation.TotalUnits(lengthOfLine, sizeOfUnit, gap);
                float angledAligned = guide.transform.rotation.eulerAngles.y - 90;
                List<Vector3> positions = GFprocedure.PositionsOnRadius(I, E, sizeOfUnit, gap, angledAligned);
                int totalUnitsPlaced = PlaceUnits(positions);

                debugLine(positions.Count, totalUnitsPlaced);

                Debug.Log(E);
            }
        }

        private Vector3 ClickedCoord()
        {
            Ray ray = mainCam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            Vector3 clickedCoord = Vector3.zero;

            if (Physics.Raycast(ray, out hit, 100.0f, groundMask))
            {
                clickedCoord = hit.point;   //Coordinates of clicked position.
            }
            return clickedCoord;
        }

        private int PlaceUnits(List<Vector3> positions)
        {
            int units = 0;
            for (int i = 0; i < positions.Count; i++)
            {
                GameObject unit = Instantiate(prefab);
                unit.transform.position = positions[i];
                units++;
            }
            return units;
        }

        private void RotateGuide()
        {
            Ray cameraRay = mainCam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            //  Coordinate
            if (Physics.Raycast(cameraRay, out hit, 100.0f, groundMask))
            {

                Vector3 pointToLook = cameraRay.GetPoint(hit.distance);
                // Rotate parent Object
                guide.transform.LookAt(new Vector3(pointToLook.x, transform.position.y, pointToLook.z));
            }
        }

        private void debugLine(int totalUnitsPos, int totalUnitsfab)
        {
            Debug.Log("[Debug Units] Total Positions: " + totalUnitsPos);
            Debug.Log("[Debug Units] Total Units Prefab: " + totalUnitsfab);
        }
    }
}

